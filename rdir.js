// This code sets the function to redirect visitors to the listed link.
function redirect(elementId, url) {
    const element = document.getElementById(elementId);
    element.onclick = function () {
        window.location.replace(url);
    }
}

// The links below will redirect visitors to the button that they clicked on.
redirect("songsdirBtn", "songs.html");
